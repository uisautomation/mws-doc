#!/usr/bin/env bash

# Turn on logging and exit at first failure
set -xe

zip_file=$1
new_site_name=$2

if [ -z "${new_site_name}" ] || [ -z "${zip_file}" ]; then
	echo "Usage: upload-new-site.sh <zip> <sitename>" >&2
	exit 1
fi

new_site_domain="${new_site_name}.bitballoon.com"

new_id=$(bitballoon create "${zip_file}")

bitballoon delete "${new_site_domain}" || echo "no existing site"

bitballoon rename "${new_id}" "${new_site_name}"
