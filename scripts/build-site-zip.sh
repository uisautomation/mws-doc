#!/usr/bin/env bash

# Turn on logging and exit at first failure
set -xe

# Find script location and move to root
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${DIR}/.."
echo "Working directory:"
pwd

# Compile docs
make html

# Zip docs
pushd _build/html
zip -r ../site.zip .
popd
