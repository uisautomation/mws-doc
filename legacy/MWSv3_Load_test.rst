MWSv3_Load_test
===============

We wanted to test the performance of our Xen Cluster in order to
discover its bottlenecks and performance peaks. We used Amazon Web
Services virtual machines and Apache Portable Runtime (APR).

If you want to use AWS at UIS please read the following wiki:
https://wiki.cam.ac.uk/uis-aws-docs/

You will need an
`account <https://wiki.cam.ac.uk/uis-aws-docs/Getting_Started>`__ and
the API `access
keys <https://wiki.cam.ac.uk/uis-aws-docs/Arcus_-_AWS_Access_Keys>`__ of
your account.

You can use an open source tool that will help you perform the load
test: https://github.com/newsapps/beeswithmachineguns

Follow the instructions of their README file and install
beeswithmachineguns. Once installed configure the .boto config file with
your API credentials.

Create an AWS image (AMI) with apr installed and use this image as the
base image of the AWS EC2 machines.

In our test we used the following command

bees up -k amc203 -z eu-west-1a -i ami-57c17624 -t m4.xlarge -n 5

I have selected my key par 'amc203' created the EC2 machines at
eu-west-1a (ireland) use the image I've created previously and launched
5 m4.xlarge VMs

Once created, we launched an attack using the following command:

bees attack -u http://URL/ -n 30000 0c 3000

This means that for each one of the VMs created for load testing
purposes (50) we launch 30000 request using 3000 connections
simultaneous.

All MWS3 clients had a copy of a Drupal installation to produce load to
the database, Apache, and PHP module. The results for the VMs attacked
was the one expected, they were DOSed. You can see two examples in the
following images:

|1.png| |2.png|

You can see how the Load increases from near 0 to 125, using all the CPU
and memory available, as well as a lot of Network and Disk Read.

From the point of view of the cluster, we couldn't monitor the total %
of CPU used as Xen can only monitor virtual CPUs associated to the
different domains and not the real CPU managed by the hypervisor (not
even at dom0 level). More research could be needed to see how we can
monitor the total CPU used. In terms of memory, others VMs were not
affected as memory is dedicated resources not shared as the CPU. In
other tests we were able to test the peak performance of our raid-SSD
achieving 3GBps. In these load test, the read/write usage of the disks
are far from the peak. This can be explained because the bottleneck of
the cluster is the network. As we can see in the following image, the
network connection achieve it its limit of ~100MBps. This may affect
other VMs in the future, if the network is fully used. Note that there
is a planned upgrade from 1Gbps to a 10Gbps network link which will
improve the situation. After this upgrade has happened, we should
perform the load test again to see what has changed.

The total amount of traffic generated was 15GB in a period of 5 minutes

.. figure:: 3.png
   :alt: 3.png

   3.png

.. |1.png| image:: 1.png
.. |2.png| image:: 2.png

