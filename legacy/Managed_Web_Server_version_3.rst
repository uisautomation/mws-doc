Managed_Web_Server_version_3
============================

Service Information
-------------------

-  `MWSv3 Panel Server Deployment
   Checklist <MWSv3_Panel_Server_Deployment_Checklist>`__

-  `MWSv3 VM Life Cycle <MWSv3_VM_Life_Cycle>`__

-  `MWSv3 Supporters Documentation <MWSv3_Supporters_Documentation>`__

   -  Metrics panels for
      `production <https://sliderule.mws3.csx.cam.ac.uk/dashboard/script/mws3arch.js?node1=agogue_csi_cam_ac_uk&node2=odochium_csi_cam_ac_uk>`__
      and
      `test <https://sliderule.mws3.csx.cam.ac.uk/dashboard/script/mws3arch.js?node1=ophon_csi_cam_ac_uk&node2=opus_csi_cam_ac_uk>`__
      clusters
   -  simple cluster status pages for
      `production <http://ial.csi.cam.ac.uk/>`__ and
      `test <http://on.csi.cam.ac.uk/>`__ clusters
   -  Minimalist monitoring via `'Platforms' Nagios
      instance <uis:Platforms_Nagios>`__ in the 'mws3'
      `hostgroup <http://beholder.csi.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=mws3&style=detail>`__
      and
      `servicegroup <http://beholder.csi.cam.ac.uk/nagios/cgi-bin/status.cgi?servicegroup=mws3&style=detail>`__

-  `MWSv3 List of Manually-created
   VMs <MWSv3_List_of_Manually-created_VMs>`__

-  `MWSv3 List of Physical machines <MWSv3_List_of_Physical_machines>`__

-  `MWSv3 IPs <MWSv3_IPs>`__

-  `MWSv3 Sysadmin and Deployment
   Notes <MWSv3_Sysadmin_and_Deployment_Notes>`__

   -  `MWSv3 Server Install <MWSv3_Server_Install>`__
   -  `MWSv3 Xen Host Patching <MWSv3_Xen_Host_Patching>`__
   -  `MWSv3 Panel Install <MWSv3_Panel_Install>`__
   -  `Disaster Recovery <MWSv3_Disaster_Recovery>`__
   -  |Specs for SSDs| (those in agogue and odochium)
   -  `MWSv3 2016 new disks <MWSv3_2016_new_disks>`__
   -  `MWSv3 Known bugs and issues <MWSv3_Known_bugs_and_issues>`__
   -  `MWSv3 Debian Stretch <MWSv3_Debian_Stretch>`__

-  `MWS Documentation Terminology <MWS_Documentation_Terminology>`__

-  `MWSv3 Load test <MWSv3_Load_test>`__

-  `MWSv3 Password arrangements <MWSv3_Password_arrangements>`__

-  Source code:

   -  `Web control
      panel <https://git.csx.cam.ac.uk/x/ucs/mwsv3/webapp.git>`__:
      ssh://ucs@git.csx.cam.ac.uk/mwsv3/webapp.git (team read/write,
      world readable)
   -  `MWS servers (web control panel) and MWS clients ansible
      roles <https://git.csx.cam.ac.uk/i/ucs/mwsv3/ansible.git>`__:
      ssh://ucs@git.csx.cam.ac.uk/mwsv3/ansible.git (team read/write,
      world readable)

-  `MWSv3 Contacting Webmasters <MWSv3_Contacting_Webmasters>`__

-  `MWSv3 Resource usage <MWSv3_Resource_usage>`__

-  `Archived documents from old MWS
   wiki <Archived_documents_from_old_MWS_wiki>`__

Development / Project Manglement / Planning Information
-------------------------------------------------------

-  `MWSv3 Project Plan <MWSv3_Project_Plan>`__

-  `MWSv3 survey of existing MWS
   webmasters <MWSv3_survey_of_existing_MWS_webmasters>`__

-  `Proposed features <Media:MWSv3-proposed-architecture.pdf>`__

-  `MWSv3 List of use cases <MWSv3_List_of_use_cases>`__

-  `MWSv3 VM arquitecture and
   infrastructure <MWSv3_VM_arquitecture_and_infrastructure>`__

-  `MWSv3 Architecture <MWSv3_Architecture>`__

-  `MWSv3 Control panel <MWSv3_Control_panel>`__

Release planning
----------------

-  `MWSv3 Release announcements <MWSv3_Release_announcements>`__

-  `MWSv3 Use inside UIS <MWSv3_Use_inside_UIS>`__

Documentation Development
-------------------------

-  `MWSv3 DNS-related documentation <MWSv3_DNS-related_documentation>`__
   -- now published:
   http://mws-help.uis.cam.ac.uk/mws3-fundamentals/mws-hostnames;
   http://mws-help.uis.cam.ac.uk/billing/mws-dns-management
-  `MWSv3 Wordpress notes <MWSv3_Wordpress_notes>`__

.. |Specs for SSDs| image:: Ssd-dc-s3610-spec.pdf

