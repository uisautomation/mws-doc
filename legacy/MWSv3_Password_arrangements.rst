MWSv3_Password_arrangements
===========================

SSH access to MWSv3 sites uses UIS passwords. MWSv3 is a client of
passwords.csx and so receives new passwords as and when people set them.
To save existing users for having to do a password change (or at least
non-change) the MWSv3 password store was seeded from Raven's password
database.

To reduce exposure, MWSv3's full password store isn't backed up. In the
event of total loss it can be recreated from Raven. This script (or
something like it) will extract passwords from the Raven password
database:

| `` #!/usr/bin/env python``
| `` ``
| `` import MySQLdb``
| `` ``
| `` db = MySQLdb.connect(host='localhost',``
| ``                      db='raven',``
| ``                      read_default_file=``\ “``~/.my.cnf``”\ ``)``
| `` cursor = db.cursor()``
| `` cursor.execute(``\ “``SELECT``\ `` ``\ ``id,pwd``\ `` ``\ ``FROM``\ `` ``\ ``users``”\ ``)``
| `` for row in cursor.fetchall():``
| ``     print ``\ “``%s\t%s``”\ `` % row``

It needs to be run by someone with at least read access to the database,
as it stands based on a ~/.my.cnf file. Obviously its output needs to be
treated carefully.
