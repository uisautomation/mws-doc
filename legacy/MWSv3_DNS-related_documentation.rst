MWSv3_DNS-related_documentation
===============================

Hostnames on the Managed Web Server
-----------------------------------

::

    All websites are identified by one or more 'hostnames', sometimes called
    'DNS Names', 'Domains' or 'Domain names'. These appear in URLs between the initial
    double slash and the next slash, so 'www.ucs.cam.ac.uk' in the URL
    http://www.ucs.cam.ac.uk/managed-web-service.

    You don't have a free choice about what hostname to use. 

    The University normally uses hostnames that end '.cam.ac.uk'. 
    Within this departments, colleges, etc. use names with their own naming suffixes; for example
    'botolph.cam.ac.uk' for the fictional St Botolph's College. The
    easiest thing to do on the MWS Managed Web Server is to use a hostname
    with an existing suffex, for example 'project.botolph.cam.ac.uk'. If
    you want to do this you might want to discuss your plans with the IT
    staff in the relevant institution.

    For testing, there is a special suffix usertest.mws3.csx.cam.ac.uk in
    which you can use any reasonable hostname providing it's not already in
    use. You should remove such hostnames once they are no longer needed.

    New names directly within 'cam.ac.uk' are only created 
    very occasionally. This is governed by strict guidelines overseen by the
    Information Services Committee. If you think you might be entitled to
    one, please discuss this with Institution Strategy in University
    Information Services (email institution-strategy@uis.cam.ac.uk). See
    [1] for more information.

    You can also use hostnames with other endings,
    e.g. botolph.example.org. To do this you need to obtain the right to
    use the names, probably by buying it from a 'Domain
    Registrar'. Domain Registrars normally also provide 'Domain Name
    System' (DNS) services for the names that they sell. University
    Information Services' 'Managed Zone Service' may be able to handle this
    for you, see [2] for further detail.

    On the MWS, hostnames are used for two different but related things:

    * [LINK] Routing requests to the right MWS server
    * [LINK] Routing requests to the right Web Site 

    [1] http://www.ucs.cam.ac.uk/instinfo/domain-names/domainnames
    [2] http://www.ucs.cam.ac.uk/managed-zone-service/e/

Routing requests to MWS servers
-------------------------------

::

    When someone browses to a URL, their browser extracts the hostname from
    the URL and uses the 'Domain Name System' (DNS) to locate the corresponding 
    server. For this to work, the hostname or hostnames
    associated with each MWS Web Site need to be registered in the DNS. 

    For most hostnames within 'cam.ac.uk', this hapens automatically when you
    associate the hostname with an MWS Web Site (see [Routing requests to MWS
    Web Sites]).

    For most simple requests the administrators responsible for the name you have chosen will be
    asked to approve your request and you can expect to hear the outcome
    within a few days. If the hostname is already in use it may not be
    possible to set it up automatically and you will need to discuss your
    requirements with the relavent administrator (probably the IT staff of
    the associated department). For more technical details of this process
    see [MWS DNS management].

    For hostnames ending in somethign other than 'cam.ac.uk' you will need to do the
    registration yourself, or get someone to do it for you. Your Domain
    Registrar will probably provide a control panel that lets
    you manage your hostnames along with instructions. See [1]
    for what to do for names handled by the UIS Managed Zone Service.

    Ideally you should set up your selected hostname as 'CNAME' (i.e. alias)
    for the underlying hostname of your MWS server. You can find this on the
    front page of the server's MWS Control Panel [picture] - e.g.

.. figure:: MWS-cname.png
   :alt: MWS-cname.png

   MWS-cname.png

::

    Because of DNS limitations you can't do this for some hostnames (bare
    domain, zone apex domain, root domain), and for these you need to set
    up your selected hostname to be the IPv4 (and IPv6 if supported) addresses
    of your MWS server. You can also find these addresses on the front
    page of the server's MWS Control Panel [picture]. On many registrar
    systems the symbol '@' is used to stand for the domain itself - e.g.

|MWS-A.png| |MWS-AAAA.png|

::

    Note that 'A' and 'AAAA' DNS entries are not supported by the managed
    Zone Service.

    In all cases (hostnames within 'cam.ac.uk' and outside), changes 
    to the DNS take time to become visible. Allow at
    least a couple of hours after a change is approved or made before
    expecting it to be visible from browsers.

Routing requests to MWS Web Sites
---------------------------------

::

    When someone browses to a URL on your server, it uses the hostname from
    the URL to select the Web Site that will handle the request.

    For this to work, MWS Web Sites need to be associated with the hostnames
    that they handle. You do this from the 'Server Settings' -> 'Web
    Sites' pane of the control panel using the 'link' [picture] icon for
    the relevant site. Sites without at least one hostname won't be accessible from
    browsers.

    You don't have a free choice about the hostnames you use - see [Hostnames on
    the Managed Web Server] for more information.

    If you add or remove a hostname ending '.cam.ac.uk' then the necessary
    changes will be made to the Domain Name System (DNS) to bring the hostname
    into use. For other hostnames you will have to arrange for the
    necessary DNS updates. See [Routing requests to the right MWS server]
    for more details.

    All MWS Servers have one site called 'default'. Any requests arriving
    for hostnames not assigned to any other Web Site will be handled by the
    'default' site. This site also always responds to the underlying hostname
    of your MWS Server, e.g. mws-12345.mws3.csx.cam.ac.uk, so you can
    always browse to it using the corresponding URL which would be
    http://mws-12345.mws3.csx.cam.ac.uk/ in this example.

    Within the configuration of each Web Site, one hostname is marked as the
    'main hostname'. Any requests handled by this site for hostnames other
    than the main one will be redirected to a corresponding URL 
    containing the main hostname. This helps to promote one hostname as 
    the 'official' name of the site.

MWS DNS Management
------------------

::

    Administrators of MWS servers can set up the hostnames
    (a.k.a. DNS names, domains, or domain names) associated with the web
    sites hosted on their servers using the MWS Control Panel. To take
    effect, these hostnames also need to appear in the DNS.

    For hostnames in domains outside cam.ac.uk the MWS just provides generic
    instructions for what the domain's owner needs to do. For hostnames within
    the cam.ac.uk domain the MWS will, where possible, make the necessary
    changes using a direct interface to IP register database. This is
    consistent with the manual process used by the previous version of the MWS
    and on Falcon.

    When a new hostname within cam.ac.uk is requested the following happens:

    * Names not within existing cam.ac.uk domains are rejected.

    * Otherwise an email is sent to the contact address for the IP
      Register Management Zone (mzone) containing the proposed hostname. The
      email includes a link that can be used to approve or reject the
      request. In all cases, if the request is rejected no changes are
      made to the DNS.

        * If the request is approved and the proposed hostname doesn't already
          exist then an appropriate CNAME record is automatically
          created. If no response is received in three days the request is
          assumed to be approved.

        * If the request is approved and the proposed hostname exists and is a
          CNAME then the CNAME record is automatically amended. If no
          response is received in three days the request is assumed to be
          approved.

        * If the proposed hostname exists and is something other than a CNAME
          the request cannot be approved as it stands. To approve the
          request, mzone administrators must first either delete the
          existing record or convert it to a CNAME. If no response is
          received in three days the request is assumed to be rejected.

    In all cases the MWS administrators who made the original request will
    be notified of the outcome.

    When a hostname within cam.ac.uk is removed from a Managed Web Server site
    the corresponding CNAME record will be deleted providing it does
    actually point to a MWS site. No notification is sent when this
    happens.

    Following any update it can take up to an hour for changes to appear
    on the authoritative servers and up to a further hour to propagate to
    other caching servers.

.. |MWS-A.png| image:: MWS-A.png
.. |MWS-AAAA.png| image:: MWS-AAAA.png

