MWSv3_Supporters_Documentation
==============================

Supporters have an special section in the web application where they can
manage other people's `MWS3 <MWS3>`__ servers:
https://panel.mws3.csx.cam.ac.uk/searchadmin/

In this page they can search for a MWS3 Server by

-  MWS3 Server name
-  Hostname of any virtual machine that is part of a MWS3 Server
-  Service name or domain name associated to any of the websites
   (vhosts) of any MWS3 Server
-  User

In the first three cases, the supporter will get a list of MWS3 Severs
matching the query. For the last case, the supporter will get a list of
MWS3 Servers where the user is authorised either as an Admin or as a SSH
User.

Results
-------

From the results list the supporter will be able to reenable disabled
sites or unban administratively suspended sites. They will also be able
to access to the MWS3 Server control panel as if they were an admin of
that MWS3 Server.

Disabled sites
--------------

If the site is disabled the supporter won't be able to access the
control panel. The reason for this is that in these cases the underlying
virtual machine is powered off and thus any change in the control panel
won't be applied. If you want to access to the control panel you will
have to re-enabled it before and then disable it later if you want it to
keep it in the same state of how you found it.
