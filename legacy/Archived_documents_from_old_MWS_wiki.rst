Archived_documents_from_old_MWS_wiki
====================================

Various initial planning documents (some details of which were
subsequently overtaken by events):

-  (original) `MWS Project proposal <MWS_Project_proposal>`__
-  `MWS Project announcement email <MWS_Project_announcement_email>`__
-  (original) `MWS Architecture summary <MWS_Architecture_summary>`__
