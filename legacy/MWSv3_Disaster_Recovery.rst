MWSv3_Disaster_Recovery
=======================

Introduction
------------

This page describes how to restore the panel server and
`mws3 <Managed_Web_Server_version_3>`__ guests in the event of total
loss of data. It shouldn't be necessary (since the panel shouldn't fail,
and we have snapshots for user backups), but you never know...

These instructions are for wholesale restores; for smaller things, you
can probably just copy individual files / directories around.

**Bugs**: This would be smoother if the guest restore happened before
the first Ansible run - when that becomes possible, these instructions
will need updating. Also, there should be a button on the panel for the
MWS3 team to run Ansible against a particular guest.

Panel Restore
-------------

This is complicated by the fact that you're restoring ``/`` - doing this
in the obvious way will result in ``restore`` SEGVing in the middle of
the process. And there's no easy way to get ``restore`` within the
Debian installer (we could try building a udeb and so on, but it's
probably not worth it). So, the approach is:

Boot an installer
~~~~~~~~~~~~~~~~~

Either install the system (if needed) or boot into rescue mode of the
installer. Select the American English keyboard layout (otherwise you
won't have \| ).

Launch a shall in the target
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From the installer, you can start a shell and then ``chroot /target``;
the rescue image has an option to start a shell in the target.

Install “dump”
~~~~~~~~~~~~~~

``apt-get install dump``. This gives you the ``restore`` command.

Make a staging area
~~~~~~~~~~~~~~~~~~~

``mkdir /staging``

Do the restore
~~~~~~~~~~~~~~

You need to restore the full dump, then any incrementals - and remove
the ``restoresymtable`` file after you're done. Currently, this is being
done via a staging are on ``ghast``, but we may move to a different
location in due course.

| ``cd /staging``
| ``ssh mcv21@ghast.csi.cam.ac.uk ``\ “``cat``\ `` ``\ ``/export/scratch/mws3-restore/test.dev.mws3.csx.cam.ac.uk.full._``”\ `` | restore -r -f -``
| ``ssh mcv21@ghast.csi.cam.ac.uk ``\ “``cat``\ `` ``\ ``/export/scratch/mws3-restore/test.dev.mws3.csx.cam.ac.uk.incr._``”\ `` | restore -r -f -``
| ``rm restoresymtable``

Move the restored contents into place
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is best done \*outside\* of the chroot (to avoid over-writing
libraries and the like you're still using!); remove the old directory
and mv the new one into place:

| ``cd /target/scratch``
| ``for i in * ; do if [ $i != ``\ “``dev``”\ `` ]; then rm -rf /target/$i ; mv $i /target/ ; fi ; done``

Update fstab
~~~~~~~~~~~~

``/etc/fstab`` mounts filesystems by UUID, and these UUIDs will have
been changed by the re-install. So you'll need to edit ``/etc/fstab``
accordingly. To find out what the UUIDs are now, run the ``blkid``
command.

Update bootloader
~~~~~~~~~~~~~~~~~

Run ``update-initrd`` to make sure the initrd is trying to mount the
correct filesystems, then ``update-grub`` and/or ``grub-install``
(depending on whether this is a fresh install or a rescue-mode of a
previously-bootable install).

Guest Restore
-------------

In the normal run of things, you can simply extract things from a
previous snapshot - this procedure is for if something catastrophic has
gone wrong, and you need the ``oook`` backup. This is easier, as all the
data we back up is in ``/replicated``

Re-Install Guest
~~~~~~~~~~~~~~~~

Visit /admin of the relevant panel, click “Virtual machines”, click the
checkbox near the relevant machine, select “recreate VM” from the menu
at the bottom of the list, and click “Go”. Then Click on the service
address (the right-most column entry), and refresh that page from time
to time until the state becomes “Ready”. **NB**: this runs Ansible once,
which may fail.

Restore data
~~~~~~~~~~~~

Stop mysql before you do this.

| ``service mysql stop``
| ``cd /replicated ``
| ``ssh mcv21@ghast.csi.cam.ac.uk ``\ “``cat``\ `` ``\ ``/export/scratch/mws3-restore/mws-priv-16.mws3.csx.cam.ac.uk.full._``”\ `` | restore -r -f -``
| ``ssh mcv21@ghast.csi.cam.ac.uk ``\ “``cat``\ `` ``\ ``/export/scratch/mws3-restore/mws-priv-16.mws3.csx.cam.ac.uk.incr._``”\ `` | restore -r -f -``
| ``rm restoresymtable``
| ``service mysql start``

You **may** need to remount ``/home`` after doing this (if that's the
case, you can tell because ``/home`` will be empty. It is probably not
necessary to ``rm -rf /replicated/*`` before you start, but if you don't
``restore`` will not over-write anything that's present (and will tell
you this).

Run Ansible
~~~~~~~~~~~

There should be a button for this, but there isn't, so log into the
panel as root, ``su`` to mws-admin, and:

| ``cd ~/ansible``
| ``ansible-playbook -l mws-priv-16.mws3.csx.cam.ac.uk -i ../bin/userv_inventory.sh mwsclients.yml``
