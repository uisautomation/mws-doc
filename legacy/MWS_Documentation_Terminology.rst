MWS_Documentation_Terminology
=============================

MWSv3 teminology
================

Agreed terminology (in **bold**) for MWS3

-  The overall name of this is the **Managed Web Service** (or **MWS**
   for short).
-  A customer of the *Managed Web Service* gets a **MWS Server**. This
   is provided by a 'Virtual Machine' ('VM') running on the *Managed Web
   Service* infrastructure.
-  Each *MWS Server* hosts one or more **Web Sites** (a.k.a. Virtual
   Hosts or VHost) that are associated with one or more host names in
   the DNS.
-  Each *MWS Server* actually consists of two **Instances** -- a
   **Production Instance** that hosts *Web sites* for production use and
   a **Test Instance** for testing updates and changes.
-  A customer manage their *MWS Server* via the **MWS Control Panel**.
-  Each *MWS Server* has at least one (preferably more)
   **Administrators** who are responsible for the server's operation,
   have access to the *MWS Control Panel* and have SSH access to the
   server. The **Users** of an *MWS Server* are the *Administrators* and
   anyone else with SSH access to the server or to part of it (some CMSs
   and web applications may have their own idea of 'users' which are
   separate from this).
-  Behind the scenes, the **Managed Web Service** consists of one or
   more **Clusters** (``mws-pacemaker`` role), each containing two **Xen
   Hosts** (``mws-xenhost`` role) and a **Quorum Server**. *Xen Hosts*
   run **Guests** (``mws-clients`` role) implimenting the *Production
   Instances* and *Test Instances*. An additional server, the **MWS
   Server** (``web-panel`` role) hosts the **MWS Control Panel**.

Proposed terminology for talking about the DNS in the context of the MWS

-  MWS *Web Sites* have associated **hostnames** (a.k.a. 'host names',
   'DNS Names', 'Domains' or 'Domain names') by which the sites are
   accessed.
-  *Hostnames* 'end with' or 'have a' **suffix** - e.g. .cam.ac.uk or
   .botolph.cam.ac.uk or .example.com (always written with a leading
   '.')
-  *Hostnames* are **registered** in the **Domain Name System**
   (**DNS**). Web browsers lookup *hostnames* in the *DNS* to find out
   how to contact the corresponding server.
-  Most *hostnames* ending .cam.ac.uk are managed by the **UIS IP
   Register Database**. Names in the database are allocated to
   **Management Zones** (**mzones**), each with a set of delegated
   managers.
-  *Hostnames* that don't end .cam.ac.uk are managed by **Domain
   Registrars**. The **UIS Managed Zone Service** can act as a *Domain
   Registrar* for such names in many cases.
