MWSv3_List_of_Manually-created_VMs
==================================

VMWare Platforms VMs
====================

Some VMs belong to MWSv3, but aren't the automatically-generated client
VMs. These are listed here so we can keep track of them:

-  boarstall.csi.cam.ac.uk / demo.dev.mws3.csx.cam.ac.uk /
   panel.mws3.csx.cam.ac.uk [Production control panel machine]
-  test.dev.mws3.csx.cam.ac.uk [Development control panel machine]
-  on.csi.cam.ac.uk [Xen MWS3 quorum server]
-  qui.csi.cam.ac.uk [Xen MWS3 quorum server]
-  ial.csi.cam.ac.uk [production quorum server]

old:

-  hale.csx.cam.ac.uk [Demonstration client machine]
-  garmr.csi.cam.ac.uk [Potential LDAP server]

Xen InfoSys VMs
===============

xen.mws3.csx.cam.ac.uk is a vbox

| name: xen.mws3.csx.cam.ac.uk
| domain: csx.cam.ac.uk
| purpose: Xen cluster for MWS3

with associated boxes:

-  ophon.csi.cam.ac.uk
-  opus.csi.cam.ac.uk

with no associated addresses

hosting vboxes:

| address: 131.111.8.225
| address: 2001:630:212:8::8c:d002
| hostname: ophobe.csi.cam.ac.uk

| address: 131.111.8.228
| address: 2001:630:212:8::8c:d003
| hostname: ix.csi.cam.ac.uk

| address: 131.111.8.229
| address: 2001:630:212:8::8c:d004
| hostname: written.csi.cam.ac.uk

| address: 131.111.8.230
| address: 2001:630:212:8::8c:d005
| hostname: wrangler.csi.cam.ac.uk

| address: 131.111.8.244
| address: 2001:630:212:8::8c:d006
| hostname: viva.csi.cam.ac.uk
