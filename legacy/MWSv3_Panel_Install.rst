MWSv3_Panel_Install
===================

Introduction
------------

These a brief notes on how to install an
`MWSv3 <Managed_Web_Server_version_3>`__ panel server. It's basically
just a case of running ansible with the mwsserver role, with a couple of
issues round secrets

TLS key and certificate
~~~~~~~~~~~~~~~~~~~~~~~

These live in ``/data/certs`` and ``/data/keys``. Extract these from the
backups of the previous instance.

send\_nsca configuration
~~~~~~~~~~~~~~~~~~~~~~~~

For the passive nagios checks to work, you need to configure
``/etc/send_nsca.cfg`` to know the credentials for Platforms' Nagios
instance. The relevant things to put in that file are documented
`here <Platforms_Nagios#Passive_checks_using_NSCA>`__.
