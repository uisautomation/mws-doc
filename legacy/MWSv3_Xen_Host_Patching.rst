MWSv3_Xen_Host_Patching
=======================

Introduction
------------

In general, you can just patch the xen hosts in the usual manner of
``apt-get update && apt-get dist-upgrade``; for more disruptive patches
(e.g. for security issues in linux or Xen), where you need to reboot,
it's important to make sure there aren't guests running on the host
before rebooting it!

So, do this to each Xen host in turn:

#. ``crm_mon -1``

   -  check for any errors and make sure they won't cause trouble
   -  specifically, if a resource has errors on one node, Pacemaker may
      stop it rather than trying again to run it on that node
   -  ``crm resource cleanup`` is probably helpful here

#. Adjust the scores of the two nodes to deprioritise the one you're
   patching (e.g. to patch agogue):

   -  ``crm node attribute agogue set node-score 0``
   -  ``crm node attribute odochium set node-score 100``
   -  you can use ``crm_mon`` to monitor the location of guests, also
      the relevant quorum server's web-version
   -  ``xl list`` will show only Domain-0 once this process is complete

#. ``apt-get update && apt-get dist-upgrade``
#. reboot

   -  ``shutdown -r now 'useful message'`` or similar
   -  if you want, visit the machine's virtual console to observe the
      reboot cycle (the hardware takes a while to reboot)
   -  check things are happy with ``systemctl status`` and
      ``drbd-overview``

#. Re-adjust node scores:

   -  ``crm node attribute odochium set node-score 0``
   -  This should cause Pacemaker to rebalance the VMs between the
      nodes.

If you want to move a small number of guests back to the patched machine
to test it you can do so by setting a location constraint for that guest
with a score higher than 100. You have to do this by editing the running
CRM configuration (e.g. to push mws-priv-30 back to agogue):

::

    # crm configure
    crm(live)configure# edit
    ...using the editor, add a line 'location temp mws-priv-7 200: agogue' and save the result...
    crm(live)configure# verify
    crm(live)configure# commit
    crm(live)configure# quit

Re-edit the configuration to remove the rule when you have finished.
