MWSv3_List_of_Physical_machines
===============================

mwsxencluster0 - main development cluster
-----------------------------------------

| ``address:     131.111.8.217``
| `` address:     2001:630:212:8::8d:1``
| `` hostname:    opus.csi.cam.ac.uk``
| `` ``
| ``address:     131.111.8.218``
| `` address:     2001:630:212:8::8d:2``
| `` hostname:    ophon.csi.cam.ac.uk``

With quorum server on.csi.cam.ac.uk

mwsxencluster2 - production cluster
-----------------------------------

| ``address:     131.111.8.30``
| `` address:     2001:630:212:8::8d:6``
| `` hostname:    agogue.csi.cam.ac.uk``
| `` ``
| ``address:     131.111.8.32``
| `` address:     2001:630:212:8::8d:7``
| `` hostname:    odochium.csi.cam.ac.uk``

With quorum server ial.csi.cam.ac.uk

mwsxencluster1 - old test/dev cluster
-------------------------------------

| ``address:     131.111.8.225``
| `` address:     2001:630:212:8::8d:3``
| `` hostname:    ophobe.csi.cam.ac.uk``
| `` ``
| ``address:     131.111.8.228``
| `` address:     2001:630:212:8::8d:4``
| `` hostname:    ix.csi.cam.ac.uk``

With quorum server qui.csi.cam.ac.uk
