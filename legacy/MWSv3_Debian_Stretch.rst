MWSv3_Debian_Stretch
====================

Debian Stretch was released on 17 June 2017, and the end of full support
for Jessie has been announced for ~6 June 2018:
https://wiki.debian.org/DebianReleases#Current_Releases.2FRepositories.

https://wiki.debian.org/NewInStretch

and
https://www.debian.org/releases/stretch/amd64/release-notes/ch-whats-new.en.html

The linux kernel goes from 3.16 series to 4.9 series.

PHP
---

Stretch has PHP 7.0 (as opposed to 5.6 in Jessie).

There is documentation on the changes that may be needed for PHP 7.0 in

http://php.net/manual/en/migration70.php

and

https://wiki.php.net/rfc/remove_deprecated_functionality_in_php7

Known sites likely to be affected:

noggin.pdn.cam.ac.uk: unless they have updated pmwiki, they'll be
affected by changes to preg\_replace (the /e modifer has been deprecated
since 5.5, and support for it has been removed in 7.0). Advice is to use
preg\_replace\_callback instead:

http://php.net/manual/en/function.preg-replace.php

MySQL/MariaDB
-------------

Jessie has MySQL 5.5. In Stretch all MySQL packages have been superseded
by equivalent MariaDB packages (MariaDB 10.1)

From
https://www.debian.org/releases/stretch/amd64/release-notes/ch-whats-new.en.html#mariadb-replaces-mysql

"MariaDB is now the default MySQL variant in Debian, at version 10.1.
The stretch release introduces a new mechanism for switching the default
variant, using metapackages created from the mysql-defaults source
package. For example, installing the metapackage default-mysql-server
will install mariadb-server-10.1. Users who had mysql-server-5.5 or
mysql-server-5.6 will have it removed and replaced by the MariaDB
equivalent. Similarly, installing default-mysql-client will install
mariadb-client-10.1.

Note that the database binary data file formats are not backwards
compatible, so once you have upgraded to MariaDB 10.1 you will not be
able to switch back to any previous version of MariaDB or MySQL unless
you have a proper database dump. Therefore, before upgrading, please
make backups of all important databases with an appropriate tool such as
mysqldump.

The virtual-mysql-\* and default-mysql-\* packages will continue to
exist. MySQL continues to be maintained in Debian, in the unstable
release. See https://wiki.debian.org/Teams/MySQL for current information
about the mysql-related software available in Debian."

MariaDB have a page on transitioning in Debian at
https://mariadb.com/kb/en/mariadb/moving-from-mysql-to-mariadb-in-debian-9/

They strongly recommend doing a full database backup before doing the
upgrade.

Notes on the upgrade from MySQL to MariaDB from
https://wiki.debian.org/NewInStretch :

"The upgrade from MySQL to MariaDB may require you to run the
mysql\_upgrade command. If you see errors like Incorrect definition of
table mysql.event: expected column 'sql\_mode' at position 14 to have
type set... in /var/log/mysql/error.log, this is probably your first
step.

The upgrade from MySQL to MariaDB may also require you to change user
passwords from the “old” format to the “new” format. If you get errors
like Server is running in --secure-auth mode, but 'wikiuser'@'localhost'
has a password in the old format; please change the password to the new
format upon connection, try resetting the user's password
https://mariadb.com/kb/en/library/set-password/ "

We could in theory get MySQL packages from repo.mysql.com
(http://repo.mysql.com/apt/debian/dists/stretch/ has mysql-5.6,
mysql-5.7 and mysql-8.0).

The virtual-mysql-\* packages are listed in
https://wiki.debian.org/Teams/MySQL/virtual-mysql-server, and the schema
changes to use default-mysql-\* at
https://wiki.debian.org/Teams/MySQL/default-mysql-server

Python
------

Continues to provide two versions of Python: 2.7 and 3.5 (instead of
3.4). We're currently using 2.7. The versions in stretch are 2.7.13 and
3.5.3.

We hope to be able to support both versions of Python in Stretch (we
only have support for 2.7 in Jessie).

Perl
----

goes from 5.20 to 5.24.
