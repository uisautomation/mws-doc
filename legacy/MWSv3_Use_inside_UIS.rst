MWSv3_Use_inside_UIS
====================

The 'new' (Spring 2016) `Managed Web
Service <http://www.ucs.cam.ac.uk/managed-web-service/>`__ attracts a
annual charge which depends on the size of server being used. To help
keep MWS's finances straight this charge also applies to use inside the
UIS (which we'd still like to encourage, where appropriate).

External users are expected to upload a purchase order as part of the
payment process. To reduce the administrative load, our Finance Team
have agreed that this isn't necessary for internal UIS use. Internal
users should upload a copy of an approved requisition form and the
Finance office will arrange the necessary internal transfers. The form
should show clearly the account from which the money should be
transferred, and should obviously be approved by an appropriate budget
holder.
