Source-code reposiories
=======================

Cloud-hosted repositories
-------------------------

The following components are hosted in cloud repositories:

MWS Control Panel - https://bitbucket.org/uisautomation/mws-panel
    The Django application which provides :any:`panel` along with
    :any:`vmmanager`.

MWS Ansible Configuration - https://bitbucket.org/uisautomation/mws-ansible
    Ansible configuration to deploy the test and production instances.

Getting access
``````````````

If you don't have access to any of the repositories listed above, email
**automation@uis.cam.ac.uk** to request access.

UIS on-premises repositories
----------------------------

Moving forward, we should migrate away from relying on git.uis repositories
for development. There is still some advantage in using them for deployment. The
repositories listed above are mirrored locally as outlined below.

MWS Control Panel - https://git.uis.cam.ac.uk/i/uis/mwsv3/webapp.git
    This is a pure mirror of the main control panel repository above.
    **Importantly, changes to this repository will be overwritten when the
    control panel repository is next mirrored.**

MWS Ansible Configuration - https://git.uis.cam.ac.uk/i/uis/mwsv3/webapp.git
    The ansible configuration is not currently mirrored.
