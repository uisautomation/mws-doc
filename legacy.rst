Legacy documentation
====================

.. toctree::
    :maxdepth: 2

    legacy/Archived_documents_from_old_MWS_wiki
    legacy/Managed_Web_Server_version_3
    legacy/MWS_Documentation_Terminology
    legacy/MWSv3_2016_new_disks
    legacy/MWSv3_Architecture
    legacy/MWSv3_Contacting_Webmasters
    legacy/MWSv3_Control_panel
    legacy/MWSv3_Debian_Stretch
    legacy/MWSv3_Disaster_Recovery
    legacy/MWSv3_DNS-related_documentation
    legacy/MWSv3_IPs
    legacy/MWSv3_Known_bugs_and_issues
    legacy/MWSv3_List_of_Manually-created_VMs
    legacy/MWSv3_List_of_Physical_machines
    legacy/MWSv3_List_of_use_cases
    legacy/MWSv3_Load_test
    legacy/MWSv3_Panel_Install
    legacy/MWSv3_Panel_Server_Deployment_Checklist
    legacy/MWSv3_Password_arrangements
    legacy/MWSv3_Project_Plan
    legacy/MWSv3_Release_announcements
    legacy/MWSv3_Resource_usage
    legacy/MWSv3_Server_Install
    legacy/MWSv3_Supporters_Documentation
    legacy/MWSv3_survey_of_existing_MWS_webmasters
    legacy/MWSv3_Sysadmin_and_Deployment_Notes
    legacy/MWSv3_Use_inside_UIS
    legacy/MWSv3_VM_arquitecture_and_infrastructure
    legacy/MWSv3_VM_Life_Cycle
    legacy/MWSv3_Wordpress_notes
    legacy/MWSv3_Xen_Host_Patching
