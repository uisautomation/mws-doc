An overview of the MWS
''''''''''''''''''''''

The 'Managed Web Service' (MWS) provides University users with a versatile
infrastructure on which to run websites. In the range of services provided by
UIS to run websites it fits between a VM service, which provides hardware and
network only, and Falcon, which provides pre-installed, pre-configured,
templated content management.

.. _components:

Service Components
==================

.. _panel:

The MWS control panel webapp
----------------------------

The MWS control panel is available at https://panel.mws3.csx.cam.ac.uk/ with a
local test instance at https://test.dev.mws3.csx.cam.ac.uk/.

.. _vmmanager:

vmmanager
---------

.. note::

    This command is completely undocumented.
