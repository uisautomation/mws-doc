Managed Web Service v3 Documentation
====================================

.. toctree::
    :hidden:

    overview
    scm
    developer
    legacy

This documentation on the Managed Web Service is targeted at those who are
developing and maintaining the service. It is split into several sections:

:any:`overview`
    An overview of the MWS system.

:any:`components`
    A summary of the various components which make up the Managed Web Service.

:any:`scm`
    Where to find the various bits of source which comprise the MWSv3.

:any:`developer`
    How to modify this documentation.

:any:`legacy`
    Documentation which has been preserved from the old UCS wiki pages.
