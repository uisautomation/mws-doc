Hacking on this documentation
=============================

This documentation can be edited by anyone in the
`automation team <https://bitbucket.org/uisautomation/>`_ on Bitbucket. For
trivial edits the "Edit on Bitbucket" link at the top of the page will send you
to the Bitbucket page for the corresponding bit of the documentation and the
"Edit" button on *that* page can be used to make a change.

For more involved changes, the source can be cloned from
git@bitbucket.org:uisautomation/mws-doc.git and a pull request can be opened in
the usual manner.

Compiling the documentation locally
-----------------------------------

The source comes with a Makefile which can be used to compile the documentation
locally. Firstly check that any required Python dependencies are installed:

.. code:: bash

    $ git clone git@bitbucket.org:uisautomation/mws-doc.git
    $ cd mws-doc
    $ pip install -r requirements.txt
    $ make html

Local documentation is then available by opening ``_build/html/index.html``.
